'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Facility = mongoose.model('Facility'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  config = require('../../../mssqlconfig/config.js'),
  mssql = require('mssql');


// const config = {
//   user: 'sa',
//   password: '125768gns',
//   server: "DESKTOP-03HOR4Q",
//   database: 'CEMS-DR'
// }

/**
 * List of Users
 */
exports.list = function (req, res) {
  // Facility.find({}).sort('name').exec(function (err, facilities) {
  //   if (err) {
  //     return res.status(400).send({
  //       message: errorHandler.getErrorMessage(err)
  //     });
  //   }
  //   else {
  //     res.json(facilities);
  //   }

  // });
  var query = "select name,type,tell_am,address,EquipmentId,SMD,latitude,longitude from facility FOR JSON AUTO";
  console.log("facility query check: " + query);
  mssql.close();
  mssql.connect(config)
    .then(function () {
      console.log("sql connected");
      var request = new mssql.Request();
      request.stream = true;
      request.query(query);
      request.on('row', function (row) {
        console.log('complete');
        var obj_str = row[Object.keys(row)[0]];
        var arr = JSON.parse(obj_str);
        // console.log(row);
        console.log(arr);
        res.send(arr);
      })
    })
    .catch(function (err) {
      console.log(err);
    });

};

/**
 * User middleware  
 * 
 */
exports.facilityByID = function (req, res, next) {
  var SMD = req.body.SMD;
  console.log(SMD);
  // var id = req.body.id;
  // if (!mongoose.Types.ObjectId.isValid(id)) {
  //   return res.status(400).send({
  //     message: '고유값 형태가 잘못되었습니다.'
  //   });
  // }
  var query = "select name,type,tell_am,address,EquipmentId,SMD from facility where " + "SMD='" + SMD + "' FOR JSON AUTO";
  // Facility.findById(id).exec(function (err, facility) {
  //   if (err) {
  //     return next(err);
  //   } else if (!facility) {
  //     return next(new Error('Failed to load facility ' + id));
  //   }
  //   console.log("facility 내용 정보 :"+facility);
  //   res.json(facility);

  // });
  console.log("facility query check: " + query);
  mssql.close();
  mssql.connect(config)
    .then(function () {
      console.log("sql connected");
      var request = new mssql.Request();
      request.stream = true;
      request.query(query);
      request.on('row', function (row) {
        console.log('complete');
        var obj_str = row[Object.keys(row)[0]];
        var arr = JSON.parse(obj_str);
        // console.log(row);
        console.log(arr);
        res.send(arr);
      })
    })
    .catch(function (err) {
      console.log(err);
    });


};

/* 
  #2 type 별로 get */
exports.facilityByType = function (req, res, next) {
  var type = req.body.type;
  console.log('type은?' + type);

  // Facility.find({ 'type': type }, function (err, facilities) {
  //   if (err) {
  //     return next(err);
  //   } else if (!facilities) {
  //     return next(new Error('Failed to load facility' + type));
  //   }
  //   console.log("facilities 내용정보:"+ facilities);
  //   res.json(facilities);
  // });\
  var query = "select name,type,tell_am,address,EquipmentId,SMD from facility where " + "type='" + type + "' FOR JSON AUTO"
  // var query = "select * from facility where "+ "type='"+type+"' FOR JSON AUTO";
  console.log("facility query check: " + query);
  mssql.close();
  mssql.connect(config)
    .then(function () {
      console.log("sql connected");
      var request = new mssql.Request();
      request.stream = true;
      request.query(query);
      // request.on(testquery).then(function(row){
      request.on('row', function (row) {

        console.log('complete');
        // res.send(row);
        // console.dir(row.rowsAffected[0]);
        var obj_str = row[Object.keys(row)[0]];
        var arr = JSON.parse(obj_str);
        // res.send(arr);
        // for(var i=0; i<arr.length; i++){
        //     console.log(arr[i]);
        //     // array.push(arr[i]);
        // }
        console.log(row);
        console.log(arr);
        res.send(arr);
      })
    })
    .catch(function (err) {
      console.log(err);
    });

};
/* 
  #4 type의 카운터 get */
exports.facilityCountByType = function (req, res, next) {
  var type = req.body.type;

  Facility.count({ 'type': type }, function (err, facilities) {
    if (err) {
      return next(err);
    } else if (!facilities) {
      return next(new Error('Failed to load facility' + type));
    }
    res.json(facilities);
  });

};

/* 
  #3 all type 중복없이 가져옴 */
exports.facilityGetTypes = function (req, res, next) {

  Facility.find().distinct('type', function (err, facility) {
    if (err) {
      return next(err);
    } else if (!facility) {
      return next(new Error('Failed to load facility'));
    }
    res.json(facility);
  });

};

/* 
 modify*/
exports.modify = function (req, res, next) {
  var obj = req.body;
  obj.updated = Date.now();
  Facility.update({ '_id': obj._id }, { $set: obj }, function (err, document) {
    if (err) {
      res.status(400).json({ error: err });
    }
    else {
      res.status(200).json({ message: '성공적으로 수정하였습니다.' });
    }
  });
};

/* 
insert*/
exports.insert = function (req, res, next) {
  var obj = req.body;

  var facility = new Facility(obj);
  facility.save(function (err) {
    if (err) {
      res.status(400).json({ error: err });
    }
    else {
      res.status(200).json({ message: '성공적으로 수정하였습니다.' });
    }
  });
};

/* delete */
exports.delete = function (req, res, next) {
  Facility.remove({ _id: { $in: req.body } }).exec(function (err, facility) {
    if (err) {
      return next(err);
    } else if (!facility) {
      return next(new Error('Failed to load facility ' + req.body));
    }

    res.status(200).json({ message: '성공적으로 삭제되었습니다.' });

  });

};