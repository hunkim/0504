'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  CustomerBaseLine = mongoose.model('customerbaseline'),
  dateUtils = require('../util/dateUtils'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));


exports.getCblByEquipmentID = function (req, res, next) {
  var EquipmentId = req.body.EquipmentId;
  var mode = req.body.mode;
  CustomerBaseLine.find({ 'EquipmentId': EquipmentId,'mode':mode }, function (err, data) {
    if (err) {
      return next(err);
    } else if (!data) {
      return next(new Error('Failed to load CBL MAX4/5 Data' + EquipmentId));
    }
    res.json(data);
  });
};








