'use strict';

module.exports = function (app) {
  // User Routes
  var usages = require('../controllers/usage.server.controller');


  // app.route('/api/usages').get(usages.list);
  //app.route('/api/auth/forgot').post(users.forgot);
  app.route('/api/usage').post(usages.usageByID);
  
  app.route('/api/usagebyequipmentid').post(usages.usageByEquipmentID);
  
  app.route('/api/usagegetdata').post(usages.usageGetdata);

};
