'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  async = require('async'),
  Usage = mongoose.model('value_5min'),
  dateUtils = require('../util/dateUtils'),
  config = require('../../../mssqlconfig/config.js'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));
const sql = require('mssql');



/**
 * User middleware  
 * 
 */
exports.usageByID = function (req, res, next) {
  var id = req.body.EquipmentId;

  Usage.findById(id).exec(function (err, usage) {
    if (err) {
      return next(err);
    } else if (!usage) {
      return next(new Error('Failed to load usage ' + id));
    }
    // console.log(usage.mr_ymdhh);
    res.send(usage.EquipmentId);
  });
};

/* 
  #2 equipmentID 넣으면  */
exports.usageByEquipmentID = function (req, res, next) {
  var EquipmentId = req.body.EquipmentId;

  Usage.find({ 'EquipmentId': EquipmentId }, function (err, usages) {
    if (err) {
      return next(err);
    } else if (!usages) {
      return next(new Error('Failed to load usage' + EquipmentId));
    }
    // console.log(usages);
    res.json(usages);
  });
};

// exports.usageGetdata = function (req, res, next) {

//   var EquipmentId = req.body.EquipmentId;
//   var now = new Date();
//   var oneWeekBefore = new Date();
//   oneWeekBefore.setDate(now.getDate() - 7);
//   var start = dateUtils.yyyymmdd(now);
//   var end = dateUtils.yyyymmdd(oneWeekBefore);

//   Usage.find({ 'EquipmentId': EquipmentId, 'mr_ymdhh': { $gt: end, $lte: start } }).sort([['mr_ymdhh', 1]]).sort([['mmIndex', 1]]).exec(function (err, usages) {
//     if (err) {
//       return res.status(400).send({
//         message: errorHandler.getErrorMessage(err)
//       });
//     }
//     res.json(usages);
//   });
// }
exports.usageGetdata = function (req, res, next) {

  var EquipmentId = req.body.EquipmentId;
  console.log(EquipmentId);
  // var now = new Date();
  var test = new Date('2018-01-24');
  var oneWeekBefore = new Date();
  // oneWeekBefore.setDate(now.getDate() - 7);
  oneWeekBefore.setDate(test.getDate() - 7);
  // var start = dateUtils.yyyymmdd(now);
  var start = dateUtils.yyyymmdd(test);
  // var end = dateUtils.yyyymmdd(oneWeekBefore);
  var end = dateUtils.yyyymmdd('2018-01-30');
  console.log(start, end);
  var testquery;
  testquery = "select * from TB_DR_LP_DATA_5MIN where " + "EquipmentId='" + EquipmentId + "' and mr_ymdhh<='" + end + "' and mr_ymdhh>='" + start + "' ORDER BY mr_ymdhh, mmIndex asc FOR JSON AUTO";
  console.log(testquery);
  sql.close();
  sql.connect(config)
    .then(function () {
      console.log("connected");
      var request = new sql.Request();
      request.stream = true;
      request.query(testquery);
      // request.on(testquery).then(function(row){
      request.on('row', function (row) {

        console.log('complete');
        // res.send(row);
        // console.dir(row.rowsAffected[0]);
        var obj_str = row[Object.keys(row)[0]];
        var arr = JSON.parse(obj_str);
        // res.send(arr);
        // for(var i=0; i<arr.length; i++){
        //     console.log(arr[i]);
        //     // array.push(arr[i]);
        // }
        res.send(arr);
      })
    })
    .catch(function (err) {
      console.log(err);
    });
}





