(function () {
    'use strict';

    angular
        .module('app.prototype')
        .controller('LineChartController', LineChartController);

    LineChartController.$inject = ['$scope', 'ChartData', '$timeout', 'Colors', '$http', 'dateUtils'];
    function LineChartController($scope, ChartData, $timeout, Colors, $http, dateUtils) {
        
        var vm = this;

        var dataSet_usage = [];
        var config;
        // var ctx = document.getElementById("myChart");
        // var myChart = '';

        $scope.lastChartState;


        var object = new Object();
        var Linechart = {
            Mode: { RAW: 1, HOUR: 12, HALF: 6, QUARTER: 3 }
        };
        $scope.$on('facilityData', function (event, data) {
            vm.facilityData = data;
            console.dir(vm.facilityData);
            activate(Linechart.Mode.RAW);
            drawChart(document.getElementById("myChart"),config);
        });

        function activate(mode) {
            vm.dataSet = buildChartDataSet(mode);
            config = bulidChartOptions(mode);
        }


        function buildChartDataSet(mode) {
            var usageValue;
            var cblValue;
            var dataSet = [];
            var usageData = [];
            var cblData = [];
            switch (mode) {
                case Linechart.Mode.HOUR:
                    usageValue = vm.facilityData.valueDataHour;
                    cblValue = vm.facilityData.cblDataHour;
                    break;
                case Linechart.Mode.HALF:
                    usageValue = vm.facilityData.valueDataHalfHour;
                    cblValue = vm.facilityData.cblDataHalf;
                    break;
                case Linechart.Mode.QUARTER:
                    usageValue = vm.facilityData.valueDataQuaterHour;
                    cblValue = vm.facilityData.cblDataQuater;
                    break;
                case Linechart.Mode.RAW:
                    usageValue = vm.facilityData.usage;
                    break;
            }
            $scope.lastChartState = mode;
            var nowIndex=dateUtils.timeToIndex(Date.now());
            if (mode !== Linechart.Mode.RAW) {

                usageData.xaxis = []; //x축 label : index (임시) 
                usageData.value = []; //인덱스제외 array : 사용량 값 
                usageData.indexTotime = [];

                usageData.label = '실제사용량';
                usageData.color = '#004e66'; 
                usageData.data=[];
                
                cblData.label = 'CBL MAX(4/5)';
                cblData.color = '#E71D36';
                cblData.data=[];
                
                cblData.value = []; 

                var tmpData;
                for (var i = 0; i < parseInt(288 / mode); i++) {
                    if(i<=parseInt(nowIndex/mode)){
                        var timeString;
                        var minute = (i * (mode * 5));
                        var cblDataObject = [];
                        var usageDataObject = [];

                        usageData.xaxis.push(i);
                        usageData.value.push(usageValue[i]);
                        usageData.indexTotime.push(dateUtils.hhmm(i*mode));

                        cblData.value.push(cblValue[i]);

                        cblDataObject.push(i);
                        usageDataObject.push(i);
                        cblDataObject.push(cblValue[i]);
                        usageDataObject.push(usageValue[i]);
                        cblData.data.push(cblDataObject);
                        usageData.data.push(usageDataObject);
                    }
                }
                dataSet.push(usageData);
                dataSet.push(cblData);
                return dataSet;
                
            }
            else {
                //raw일경우만
                usageData.xaxis = []; //x축 label : index (임시) 
                usageData.value = []; //인덱스제외 array : 사용량 값 

                usageData.label = '실제사용량';
                usageData.color = '#004e66';
                usageData.data=[];
                usageData.indexTotime = [];
                for (var i = 0; i < 288; i++) {
                    if(i<nowIndex){
                        var rawData = [];

                        usageData.xaxis.push(i);
                        usageData.value.push(usageValue[i].Value_5min);
                        usageData.indexTotime.push(dateUtils.hhmm(i*mode));
                        rawData.push(dateUtils.hhmm(usageValue[i].mmIndex));
                        rawData.push(usageValue[i].Value_5min);
                        usageData.data.push(rawData);
                    }
                }
                dataSet.push(usageData);
                return dataSet;
            }
        }
        
        function bulidChartOptions(mode){

            var config = {
                type : 'bar',
                data : {
                    // labels : vm.dataSet[0].xaxis,
                    labels : vm.dataSet[0].indexTotime,
            
                    datasets : [{
                        type : 'bar',
                        label : "Usage",
                        data : [], //RAW 데이터 때문에 bulidoptions 함수에서 설정 
                        backgroundColor: 'rgba(75, 192, 192, 0.2)',
                        borderColor: 'rgba(75, 192, 192, 1)',
                        borderWidth: 1,
                        
                    },{
                        type : 'line',
                        label : "CBL Max(4/5)",
                        data : [],
                        borderColor: 'rgba(75, 00, 150,1)',
                        borderWidth: 1,
                        fill : false
                    }]
                },
                options : {
                    elements: {
                        line: {
                            tension: 0
                        }
                    },
                    
                tooltips:{
                    
                },

                    maintainAspectRatio: false,
                    hover: {mode: null},
                    maintainAspectRatio: false,
                    scales: {
                        xAxes: [{
                            type : "category",
                            id : "axis-bar",
                            display: true,
                            gridLines: {
                                display: true
                            },
                            labels: {
                                show: true,
                            },
                            ticks: {
                                beginAtZero: true,
                                stepSize: 1,
                              }
                        }],                        
            
                        yAxes: [{
                            type: "linear",
                            // display: true,
                            position: "left",
                            gridLines:{
                                display: true
                            },
                            labels: {
                                show:true,
                                
                            }
                        }]
                        
                    } 
                }
            };
            if (mode === Linechart.Mode.RAW) {
                config.data.datasets[0].data = vm.dataSet[0].value;
                
                
            }
            else if (mode === Linechart.Mode.QUARTER) {
                config.data.datasets[0].data = vm.dataSet[0].value;
                config.data.datasets[1].data = vm.dataSet[1].value;
                
            }
            else if (mode === Linechart.Mode.HALF) {
                config.data.datasets[0].data = vm.dataSet[0].value;
                config.data.datasets[1].data = vm.dataSet[1].value;
            }
            else if (mode === Linechart.Mode.HOUR) {
                config.data.datasets[0].data = vm.dataSet[0].value;
                config.data.datasets[1].data = vm.dataSet[1].value;
            }
            return config;
        }

        var myChart = null;

        function drawChart(objChart,data){
            if(myChart != null){
                myChart.destroy();
            }
            var ctx = objChart;
            myChart = new Chart(ctx,data);
        }

        $scope.raw = function () {
            if ($scope.lastChartState !== Linechart.Mode.RAW) {
                activate(Linechart.Mode.RAW);
                drawChart(document.getElementById("myChart"),config);
                   
            }
        }
        $scope.quarter = function () {
            if ($scope.lastChartState !== Linechart.Mode.QUARTER) {
                activate(Linechart.Mode.QUARTER);
                drawChart(document.getElementById("myChart"),config);
                    
            }
        }
        $scope.half = function () {
            if ($scope.lastChartState !== Linechart.Mode.HALF) {
                activate(Linechart.Mode.HALF);
                drawChart(document.getElementById("myChart"),config);
                    
            }
        }
        $scope.hour = function () {
            if ($scope.lastChartState !== Linechart.Mode.HOUR) {
                activate(Linechart.Mode.HOUR);
                drawChart(document.getElementById("myChart"),config);
            }
        }
        

       
    }
})();