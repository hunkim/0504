(function () {
    'use strict';

    angular
        .module('app.prototype')
        .controller('PrototypeGaugeController', PrototypeGaugeController);

        PrototypeGaugeController.$inject = ['$scope',  '$timeout', '$stateParams', '$http', 'dateUtils', 'facilityInfo'];
    function PrototypeGaugeController($scope,  $timeout, $stateParams, $http, dateUtils, facilityInfo) {
        var vm = this;
        

        activate();

        function activate() {
            $scope.$on('facilityData', function (event, facilityData) {
                var date = new Date();
                var chart = c3.generate({
                    data: {
                        columns: [
                            ['전력사용량',facilityData.valueDataHour[date.getHours()-1].toFixed(3)]
                        ],
                        type: 'gauge',
                    },
                    gauge: {
                        label: {
                            format: function (value, ratio) {
                                return ((value/facilityData.cblDataHour[date.getHours()-1])*100).toFixed(3)+'%';
                            },
                            show: true // to turn off the min/max labels.
                        },
                        min: 0, // 0 is default, //can handle negative min e.g. vacuum / voltage / current flow / rate of change
                        max: parseFloat(facilityData.cblDataHour[date.getHours()-1]), // 100 is default
                        units: ' ',
                        width: 39 // for adjusting arc thickness
                    },
                    color: {
                        pattern: ['#FF0000', '#F97600', '#F6C600', '#60B044'], // the three color levels for the percentage values.
                        threshold: {
                            //            unit: 'value', // percentage is default
                                    //max: 200, // 100 is default
                            values: [30, 60, 90, 100]
                        }
                    },
                    size: {
                        height: 180
                    }
                });

            });
            
        }
    }
})();