(function () {
    'use strict';

    angular
        .module('app.prototype')
        .controller('PrototypeInfoController', PrototypeInfoController);

        PrototypeInfoController.$inject = ['$scope',  '$timeout', 'dateUtils'];
    function PrototypeInfoController($scope,  $timeout, dateUtils) {
        var vm = this;
        

        $scope.$on('facilityData', function (event, facilityData) {
            $scope.info = facilityData;
            
            $scope.address = $scope.info.info.Address;
            
        });
    }
})();